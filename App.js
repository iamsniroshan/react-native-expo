import * as React from 'react';
import MapView from 'react-native-maps';
import { useFonts, FiraSans_400Regular } from '@expo-google-fonts/fira-sans';
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
} from "react-native";
import { Marker } from 'react-native-maps';
import AppLoading from 'expo-app-loading';

var dotImage = require('./assets/pin.png')



export default function App() {
  let [fontsLoaded] = useFonts({
    FiraSans_400Regular,
  });

  if (!fontsLoaded) {
    return <AppLoading />;
  } else {
    return (
      <>
        <View style={styles.container}>
          <View style={styles.headerContainer}>
            <View style={styles.headerTitle}>
              <Text style={styles.headerText}>Service radius</Text>
            </View>
            <View style={styles.headerDesc}>
              <Text style={styles.headerDescTxt} >Your home address and service radius
            determines the area from which you can receive help requests</Text>
            </View>
          </View>
          <View >
            <MapView
              style={styles.map}
              initialRegion={marker.coordinate}
              zoomControlEnabled={true}
            >
              <Marker coordinate={marker.coordinate}
                icon={dotImage}
              >

              </Marker>
              <MapView.Circle
                key={1}
                center={marker.coordinate}
                radius={200000}
                strokeWidth={1}
                strokeColor={'#49b6ad'}
                fillColor={'rgba(73, 182, 173, 0.5)'}
              >
              </MapView.Circle>
            </MapView>
          </View>
          <View style={styles.footerBar}></View>
        </View>
      </>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center'
  },
  map: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  },
  marker: {
    width: 8,
    height: 8,
    borderRadius: 4,
    backgroundColor: "rgba(130,4,150, 0.9)",
  },
  headerContainer: {
    width: '100%',
    height: 150
  },
  headerTitle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 15
  },
  headerText: {
    fontSize: 28,
    marginTop: 40
  },
  headerDesc: {
    paddingHorizontal: 15,
    paddingVertical: 10
  },
  headerDescTxt: {
    fontSize: 16,
    fontFamily: 'FiraSans_400Regular'
  },
  footerBar: {
    width: '100%',
    height: 200,
    flexDirection: 'row'
  }
});

const marker = {
  coordinate: {
    latitude: -29.1482491,
    longitude: -51.1559028,
    latitudeDelta: 50,
    longitudeDelta: 50,
  },
  title: "Best Place",
  description: "This is the best place in Portland"
}